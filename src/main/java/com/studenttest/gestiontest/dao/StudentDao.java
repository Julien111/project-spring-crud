package com.studenttest.gestiontest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.studenttest.gestiontest.model.Student;

public interface StudentDao extends JpaRepository<Student, Long> {

}

package com.studenttest.gestiontest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.studenttest.gestiontest.model.Review;

public interface ReviewDao extends JpaRepository<Review, Long> {

}

package com.studenttest.gestiontest.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.studenttest.gestiontest.model.Instructor;

public interface InstructorDao extends JpaRepository<Instructor, Long>{
	
	@Query(			
			"""
			FROM Instructor
			WHERE first_name='Marc'		
			"""
			)
	List<Instructor> findAllInstructorsHavingFirstNameMarc();


}

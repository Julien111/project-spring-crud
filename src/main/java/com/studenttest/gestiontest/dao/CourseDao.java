package com.studenttest.gestiontest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.studenttest.gestiontest.model.Course;

public interface CourseDao extends JpaRepository<Course, Long> {

}

package com.studenttest.gestiontest.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.studenttest.gestiontest.model.InstructorDetail;


public interface InstructorDetailDao extends JpaRepository<InstructorDetail, Long> {

}

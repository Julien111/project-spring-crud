package com.studenttest.gestiontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestiontestApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestiontestApplication.class, args);
	}

}

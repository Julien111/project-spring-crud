package com.studenttest.gestiontest.service;

import com.studenttest.gestiontest.dto.InstructorDetailDto;


public interface InstructorDetailService {
	
	public void saveInstructorDetail(InstructorDetailDto instructorDetailDto);	
		
	public InstructorDetailDto getInstructorDetailById(Long id);
	
	void updateInstructorDetail(InstructorDetailDto instructorDetailDto);	
	
}

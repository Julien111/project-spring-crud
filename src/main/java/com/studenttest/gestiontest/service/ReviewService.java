package com.studenttest.gestiontest.service;

import java.util.List;

import com.studenttest.gestiontest.dto.ReviewDto;
import com.studenttest.gestiontest.model.Review;

public interface ReviewService {
	
	public Review saveReview(ReviewDto reviewDto);
	
	public List<ReviewDto> listeReviews();
	
	public ReviewDto getReviewById(Long id);
	
	public boolean existsById(Long id);
}

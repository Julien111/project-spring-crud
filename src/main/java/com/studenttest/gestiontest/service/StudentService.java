package com.studenttest.gestiontest.service;

import java.util.List;

import com.studenttest.gestiontest.dto.StudentDto;
import com.studenttest.gestiontest.model.Student;

public interface StudentService {
	void saveStudent(StudentDto studentDto);
	
	void saveStudent(Student student);
	
	public List<StudentDto> listeStudents();
	
	public StudentDto getStudentById(Long id);
	
	void updateStudent(StudentDto studentDto);
	
	public boolean existsById(Long id);
	
	public void deleteStudentById(Long id);
	
	public Student getStudentTrueById(Long id);
}

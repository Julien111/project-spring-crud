package com.studenttest.gestiontest.service;

import java.util.List;

import com.studenttest.gestiontest.dto.InstructorDto;
import com.studenttest.gestiontest.model.Instructor;


public interface InstructorService {
	
	public Instructor saveInstuctor(InstructorDto instructorDto);
	
	public List<InstructorDto> listeInstructors();
	
	public InstructorDto getInstructorById(Long id);
	
	void updateInstructor(InstructorDto instructorDto);
	
	public List<Instructor> listeInstructorsEntity();
	
	public boolean existsById(Long id);
	
	public void deleteInstructorById(Long id);

}

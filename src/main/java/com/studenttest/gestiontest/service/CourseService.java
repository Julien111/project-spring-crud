package com.studenttest.gestiontest.service;

import java.util.List;

import com.studenttest.gestiontest.dto.CourseDto;
import com.studenttest.gestiontest.model.Course;

public interface CourseService {
	
	public Course saveCourse(CourseDto courseDto);
	
	public Course saveCourse(Course course);
	
	public List<CourseDto> listeCourses();
	
	public Course getCoursesById(Long id);
	
	public boolean existsById(Long id);
	
	public void deleteCourseById(Long id);
}

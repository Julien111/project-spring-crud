package com.studenttest.gestiontest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.studenttest.gestiontest.dao.StudentDao;
import com.studenttest.gestiontest.dto.StudentDto;
import com.studenttest.gestiontest.model.Student;
import com.studenttest.gestiontest.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDao studentDao;

	@Override
	public void saveStudent(StudentDto studentDto) {
		Student student = new Student();
		student.setFirstName(studentDto.getFirstName());
		student.setLastName(studentDto.getLastName());
		student.setEmail(studentDto.getEmail());
		// save
		this.studentDao.save(student);
	}

	@Override
	public List<StudentDto> listeStudents() {
		List<StudentDto> listStudents = new ArrayList<>();
		List<Student> theList = this.studentDao.findAll();

		for (Student person : theList) {
			StudentDto student = new StudentDto();
			student.setId(person.getId());
			student.setFirstName(person.getFirstName());
			student.setLastName(person.getLastName());
			student.setEmail(person.getEmail());
			listStudents.add(student);
		}

		return listStudents;
	}

	@Override
	public StudentDto getStudentById(Long id) {
		Student theStudent = studentDao.findById(id).orElse(null);
		StudentDto studentDto = new StudentDto();

		studentDto.setId(theStudent.getId());
		studentDto.setFirstName(theStudent.getFirstName());
		studentDto.setLastName(theStudent.getLastName());
		studentDto.setEmail(theStudent.getEmail());

		return studentDto;
	}
	
	@Override
	public Student getStudentTrueById(Long id) {
		Student theStudent = studentDao.findById(id).orElse(null);		
		return theStudent;
	}

	@Override
	public boolean existsById(Long id) {
		return studentDao.existsById(id);
	}

	@Override
	public void updateStudent(StudentDto studentDto) {
		Student theStudent = studentDao.findById(studentDto.getId()).orElse(null);
		theStudent.setFirstName(studentDto.getFirstName());
		theStudent.setLastName(studentDto.getLastName());
		theStudent.setEmail(studentDto.getEmail());
		studentDao.save(theStudent);
	}

	@Override
	public void deleteStudentById(Long id) {
		studentDao.deleteById(id);		
	}

	@Override
	public void saveStudent(Student student) {
		this.studentDao.save(student);		
	}

}

package com.studenttest.gestiontest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.studenttest.gestiontest.dao.InstructorDao;
import com.studenttest.gestiontest.dto.InstructorDto;
import com.studenttest.gestiontest.model.Instructor;
import com.studenttest.gestiontest.service.InstructorService;

@Service
public class InstructorServiceImpl implements InstructorService {
	
	@Autowired
	private InstructorDao instructorDao;

	@Override
	public Instructor saveInstuctor(InstructorDto instructorDto) {
		Instructor instructor = new Instructor();
		instructor.setFirstName(instructorDto.getFirstName());
		instructor.setLastName(instructorDto.getLastName());
		instructor.setEmail(instructorDto.getEmail());
		// save

		return this.instructorDao.save(instructor);		
	}

	@Override
	public List<InstructorDto> listeInstructors() {
		List<InstructorDto> listInstructors = new ArrayList<>();
		List<Instructor> theList = instructorDao.findAll();
		
		for (Instructor person : theList) {
			InstructorDto instructor = new InstructorDto();
			instructor.setId(person.getId());
			instructor.setFirstName(person.getFirstName());
			instructor.setLastName(person.getLastName());
			instructor.setEmail(person.getEmail());
			instructor.setChanel(person.getInstructorDetail().getChanel());
			instructor.setHobby(person.getInstructorDetail().getHobby());
			listInstructors.add(instructor);
		}
		
		return listInstructors;
	}
	
	@Override
	public List<Instructor> listeInstructorsEntity() {		
		List<Instructor> theList = instructorDao.findAll();		
		return theList;
	}

	@Override
	public InstructorDto getInstructorById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateInstructor(InstructorDto instructorDto) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean existsById(Long id) {
		return instructorDao.existsById(id);
	}

	@Override
	public void deleteInstructorById(Long id) {
		instructorDao.deleteById(id);			
	}

}

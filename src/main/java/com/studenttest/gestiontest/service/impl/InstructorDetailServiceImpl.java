package com.studenttest.gestiontest.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.studenttest.gestiontest.dao.InstructorDetailDao;
import com.studenttest.gestiontest.dto.InstructorDetailDto;
import com.studenttest.gestiontest.model.InstructorDetail;
import com.studenttest.gestiontest.service.InstructorDetailService;

@Service
public class InstructorDetailServiceImpl implements InstructorDetailService {
	
	@Autowired
	private InstructorDetailDao instructorDetailDao;

	@Override
	public void saveInstructorDetail(InstructorDetailDto instructorDetailDto) {
		InstructorDetail instructeurDetail = new InstructorDetail();
		instructeurDetail.setChanel(instructorDetailDto.getChanel());
		instructeurDetail.setHobby(instructorDetailDto.getHobby());
		instructeurDetail.setInstructor(instructorDetailDto.getInstructor());
		//save
		instructorDetailDao.save(instructeurDetail);		
	}

	@Override
	public InstructorDetailDto getInstructorDetailById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateInstructorDetail(InstructorDetailDto instructorDto) {
		// TODO Auto-generated method stub
		
	}

}

package com.studenttest.gestiontest.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.studenttest.gestiontest.dao.ReviewDao;
import com.studenttest.gestiontest.dto.ReviewDto;
import com.studenttest.gestiontest.model.Review;
import com.studenttest.gestiontest.service.ReviewService;

@Service
public class ReviewServiceImpl implements ReviewService {
	
	@Autowired
	private ReviewDao reviewDao;

	@Override
	public Review saveReview(ReviewDto reviewDto) {
		Review review = new Review();
		review.setComment(reviewDto.getComment());
				
		// save

		return this.reviewDao.save(review);	
	}

	@Override
	public List<ReviewDto> listeReviews() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ReviewDto getReviewById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {		
		return reviewDao.existsById(id);
	}

}

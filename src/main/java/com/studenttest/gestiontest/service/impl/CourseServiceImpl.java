package com.studenttest.gestiontest.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.studenttest.gestiontest.dao.CourseDao;
import com.studenttest.gestiontest.dto.CourseDto;
import com.studenttest.gestiontest.model.Course;
import com.studenttest.gestiontest.model.Review;
import com.studenttest.gestiontest.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
	
	@Autowired
	private CourseDao courseDao;

	@Override
	public Course saveCourse(CourseDto courseDto) {		
		//part of course
		Course course = new Course();
		course.setTitle(courseDto.getTitle());
		course.setInstructor(courseDto.getInstructor());
		//part of review
		List<Review> reviewList = new ArrayList<>();
		Review review = new Review();
		review.setComment(courseDto.getComment());
		reviewList.add(review);
		course.setListReviews(reviewList);
		// save
		return this.courseDao.save(course);		
	}

	@Override
	public List<CourseDto> listeCourses() {		
		List<CourseDto> listCourses = new ArrayList<>();
		List<Course> theList = courseDao.findAll();
		
		for (Course element : theList) {
			CourseDto course = new CourseDto();
			course.setId(element.getId());
			course.setTitle(element.getTitle());
			course.setInstructor(element.getInstructor());
			//partie review
			List<Review> comments = element.getListReviews();
			course.setComment(comments.get(0).getComment());
			listCourses.add(course);
		}
		
		return listCourses;
	}

	@Override
	public Course getCoursesById(Long id) {		
		return courseDao.findById(id).orElse(null);
	}

	@Override
	public boolean existsById(Long id) {		
		return courseDao.existsById(id);
	}

	@Override
	public void deleteCourseById(Long id) {
		courseDao.deleteById(id);		
	}

	@Override
	public Course saveCourse(Course course) {
		return this.courseDao.save(course);		
	}

}

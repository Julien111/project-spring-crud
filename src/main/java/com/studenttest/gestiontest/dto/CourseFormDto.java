package com.studenttest.gestiontest.dto;

import javax.validation.constraints.NotBlank;

public class CourseFormDto {
	
	@NotBlank	
	private String title;	
	
	private Long instructorId;	
	

	public CourseFormDto() {
		super();		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getInstructorId() {
		return instructorId;
	}

	public void setInstructorId(Long instructorId) {
		this.instructorId = instructorId;
	}

	@Override
	public String toString() {
		return "CourseFormDto [title=" + title + ", instructorId=" + instructorId + "]";
	}	
	
}

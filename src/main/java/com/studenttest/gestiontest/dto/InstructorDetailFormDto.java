package com.studenttest.gestiontest.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

public class InstructorDetailFormDto {
	
	@NotBlank
	private String firstName;	
	
	@NotBlank
	private String lastName;	
	
	@Email
	private String email;
	
	@NotBlank	
	private String chanel;
	
	@NotBlank	
	private String hobby;	
	

	public InstructorDetailFormDto() {
		super();		
	}

	public InstructorDetailFormDto(@NotBlank String firstName, @NotBlank String lastName, @Email String email,
			@NotBlank String chanel, @NotBlank String hobby) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.chanel = chanel;
		this.hobby = hobby;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getChanel() {
		return chanel;
	}

	public void setChanel(String chanel) {
		this.chanel = chanel;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}
	
	
}

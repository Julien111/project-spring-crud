package com.studenttest.gestiontest.dto;

import javax.validation.constraints.NotBlank;

import com.studenttest.gestiontest.model.Instructor;

public class CourseDto {
	
	private Long id;
	
	@NotBlank	
	private String title;	
	
	private Instructor instructor;
	
	private String comment;
	

	public CourseDto() {
		super();		
	}	

	public CourseDto(@NotBlank String title, Instructor instructor, String comment) {
		super();
		this.title = title;
		this.instructor = instructor;
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Instructor getInstructor() {
		return instructor;
	}

	public void setInstructor(Instructor instructor) {
		this.instructor = instructor;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
			

}

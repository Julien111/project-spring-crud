package com.studenttest.gestiontest.dto;

import com.studenttest.gestiontest.model.Course;
import com.studenttest.gestiontest.model.Student;

public class ChoiceCourseDtoFrom {
	
	private Student student;
	
	private Course course;
	
	public ChoiceCourseDtoFrom() {
		super();		
	}
	
	public ChoiceCourseDtoFrom(Student student, Course course) {
		super();
		this.student = student;
		this.course = course;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}	

}

package com.studenttest.gestiontest.dto;

import javax.validation.constraints.NotBlank;

public class ReviewDto {
	
	private Long id;
	
	@NotBlank
	private String comment;	

	public ReviewDto() {
		super();		
	}

	public ReviewDto(@NotBlank String comment) {
		super();
		this.comment = comment;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}		
	
}

package com.studenttest.gestiontest.dto;

import javax.validation.constraints.NotBlank;

import com.studenttest.gestiontest.model.Instructor;

public class InstructorDetailDto {
	
	@NotBlank	
	private String chanel;
	
	@NotBlank	
	private String hobby;
	
	private Instructor instructor;

	public InstructorDetailDto() {
		super();		
	}

	public InstructorDetailDto(@NotBlank String chanel, @NotBlank String hobby) {
		super();
		this.chanel = chanel;
		this.hobby = hobby;
	}

	public String getChanel() {
		return chanel;
	}

	public void setChanel(String chanel) {
		this.chanel = chanel;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}	
	
	public Instructor getInstructor() {
		return instructor;
	}

	public void setInstructor(Instructor instructor) {
		this.instructor = instructor;
	}	
	
}

package com.studenttest.gestiontest.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.studenttest.gestiontest.dto.CourseDto;
import com.studenttest.gestiontest.dto.ReviewDto;
import com.studenttest.gestiontest.service.CourseService;
import com.studenttest.gestiontest.service.ReviewService;

@Controller
public class ReviewController {
	
	private ReviewService reviewService;
	
	private CourseService courseService;	
	
	public ReviewController(ReviewService reviewService, CourseService courseService) {
		super();
		this.reviewService = reviewService;
		this.courseService = courseService;
	}

	@GetMapping("/create-review")
	public String create(Model model) {
		ReviewDto review = new ReviewDto();
		List<CourseDto> courseList = courseService.listeCourses();
		model.addAttribute("review", review);
		model.addAttribute("courseList", courseList);
		return "review/reviewCreate";
	}
	
	@GetMapping("/allreviews")
	public String allCourses(Model model) {
		List<ReviewDto> reviews = reviewService.listeReviews();
		model.addAttribute("reviews", reviews);		
		return "review/allReviews";
	}

	@PostMapping("/review/save")
	public String registrationCourse(@ModelAttribute("review") ReviewDto reviewDto, Model model) {		
		reviewService.saveReview(reviewDto);				
		return "review/success-review";
	}
	

}

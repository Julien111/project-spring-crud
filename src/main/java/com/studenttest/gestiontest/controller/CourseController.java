package com.studenttest.gestiontest.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.studenttest.gestiontest.dto.CourseDto;
import com.studenttest.gestiontest.model.Instructor;
import com.studenttest.gestiontest.service.CourseService;
import com.studenttest.gestiontest.service.InstructorService;

@Controller
public class CourseController {
	
	private InstructorService instructorService;
	
	private CourseService courseService;

	public CourseController(InstructorService instructorService, CourseService courseService) {
		super();
		this.instructorService = instructorService;
		this.courseService = courseService;
	}	
	
	@GetMapping("/create-course")
	public String create(Model model) {
		CourseDto course = new CourseDto();
		List<Instructor> instructorList = instructorService.listeInstructorsEntity();
		model.addAttribute("course", course);
		model.addAttribute("instructorList", instructorList);
		return "course/courseCreate";
	}
	
	@GetMapping("/allcourses")
	public String allCourses(Model model) {
		List<CourseDto> courses = courseService.listeCourses();
		model.addAttribute("courses", courses);		
		return "course/allcourses";
	}

	@PostMapping("/course/save")
	public String registrationCourse(@ModelAttribute("course") CourseDto courseDto, Model model) {		
		courseService.saveCourse(courseDto);				
		return "course/success-course";
	}
	
	@GetMapping("/course/delete/{id}")
	public String deleteCourse(@PathVariable("id") Long id, RedirectAttributes redirectAttrs){
		if(!courseService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "Le cours n'existe pas.");
			return "redirect:/allcourses";
		}
		else {
			courseService.deleteCourseById(id);
			return "redirect:/allcourses";
		}
	}
	

}

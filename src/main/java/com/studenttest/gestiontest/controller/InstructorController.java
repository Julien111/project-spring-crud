package com.studenttest.gestiontest.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.studenttest.gestiontest.dto.InstructorDetailDto;
import com.studenttest.gestiontest.dto.InstructorDetailFormDto;
import com.studenttest.gestiontest.dto.InstructorDto;
import com.studenttest.gestiontest.model.Instructor;
import com.studenttest.gestiontest.service.InstructorDetailService;
import com.studenttest.gestiontest.service.InstructorService;

@Controller
public class InstructorController {
	
	private InstructorService instructorService;	
	private InstructorDetailService instructorDetailService;
	
	public InstructorController(InstructorService instructorService, InstructorDetailService instructorDetailService) {
		super();
		this.instructorService = instructorService;
		this.instructorDetailService = instructorDetailService;
	}
	
	@GetMapping("/allinstructors")
	public String allinstructors(Model model) {
		List<InstructorDto> instructors = instructorService.listeInstructors();
		model.addAttribute("instructors", instructors);
		System.out.println(instructors.toString());
		return "instructor/allinstructors";
	}

	@GetMapping("/create-instructor")
	public String create(Model model) {
		InstructorDetailFormDto instructorFormDto = new InstructorDetailFormDto();
		model.addAttribute("instructorFom", instructorFormDto);

		return "instructor/instructorform";
	}

	@PostMapping("/instructor/save")
	public String registration(@ModelAttribute("instructorFom") InstructorDetailFormDto instructorDetailFormDto, Model model) {
		//partie instructeur
		InstructorDto instructorDto = new InstructorDto();
		instructorDto.setFirstName(instructorDetailFormDto.getFirstName());
		instructorDto.setLastName(instructorDetailFormDto.getLastName());
		instructorDto.setEmail(instructorDetailFormDto.getEmail());
		//partie instructeur detail
		InstructorDetailDto instructorDetailDto = new InstructorDetailDto();
		instructorDetailDto.setChanel(instructorDetailFormDto.getChanel());
		instructorDetailDto.setHobby(instructorDetailFormDto.getHobby());
		
		//on enregistre
		Instructor instructor = instructorService.saveInstuctor(instructorDto);
		instructorDetailDto.setInstructor(instructor);
		
		instructorDetailService.saveInstructorDetail(instructorDetailDto);
				
		return "instructor/success-inst";
	}
	
	@GetMapping("/instructor/delete/{id}")
	public String deleteInstructor(@PathVariable("id") Long id, RedirectAttributes redirectAttrs){
		if(!instructorService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "L'instucteur n'existe pas.");
			return "redirect:/allinstructors";
		}
		else {
			instructorService.deleteInstructorById(id);
			return "redirect:/allinstructors";
		}
	}

}

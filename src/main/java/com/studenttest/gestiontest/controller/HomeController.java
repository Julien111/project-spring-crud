package com.studenttest.gestiontest.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.studenttest.gestiontest.dto.ChoiceCourseDtoFrom;
import com.studenttest.gestiontest.dto.CourseDto;
import com.studenttest.gestiontest.dto.StudentDto;
import com.studenttest.gestiontest.model.Course;
import com.studenttest.gestiontest.model.Student;
import com.studenttest.gestiontest.service.CourseService;
import com.studenttest.gestiontest.service.StudentService;
import com.studenttest.gestiontest.validation.StudentValidationForm;

@Controller
public class HomeController {

	private StudentService studentService;
	
	private CourseService courseService;	

	public HomeController(StudentService studentService, CourseService courseService) {
		super();
		this.studentService = studentService;
		this.courseService = courseService;
	}

	@GetMapping("/")
	public String home() {
		return "home";
	}

	@GetMapping("/allstudents")
	public String allStudents(Model model) {
		List<StudentDto> students = studentService.listeStudents();
		model.addAttribute("students", students);
		return "allstudents";
	}

	@GetMapping("/create-student")
	public String create(Model model) {
		StudentDto studentDto = new StudentDto();
		model.addAttribute("student", studentDto);
		return "studentform";
	}

	@PostMapping("/student/save")
	public String registration(@ModelAttribute("student") StudentDto studentDto, Model model) {
		studentService.saveStudent(studentDto);
		return "success";
	}

	@GetMapping("/edit/{id}")
	public String editStudent(@PathVariable("id") Long id, Model model, RedirectAttributes redirectAttrs) {
		StudentDto student = studentService.getStudentById(id);

		if (!studentService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "L'étudiant n'existe pas.");
			return "redirect:/allstudents";
		}

		StudentValidationForm validForm = new StudentValidationForm();
		validForm.setFirstName(student.getFirstName());
		validForm.setLastName(student.getLastName());
		validForm.setEmail(student.getEmail());

		model.addAttribute("studentForm", validForm);
		model.addAttribute("student_id", student.getId());
		return "editform";
	}

	@PostMapping("/edit/{id}")
	public String editSubmit(@PathVariable("id") Long id,
			@Valid @ModelAttribute("studentForm") StudentValidationForm studentForm, BindingResult bindingResult,
			Model model, RedirectAttributes redirectAttrs) {
		if (bindingResult.hasErrors()) {
			model.addAttribute("studentForm", studentForm);
			model.addAttribute("student_id", id);
			return "editform";
		}
		System.out.println("test");
		StudentDto student = studentService.getStudentById(id);		
		student.setFirstName(studentForm.getFirstName());
		student.setLastName(studentForm.getLastName());
		student.setEmail(studentForm.getEmail());

		studentService.updateStudent(student);
		redirectAttrs.addAttribute("id", id).addFlashAttribute("msgSuccessEdit", "L'étudiant a bien été modifiée.");
		return "redirect:/allstudents";
	}
	
	@GetMapping("/delete/{id}")
	public String deleteStudent(@PathVariable("id") Long id, RedirectAttributes redirectAttrs){
		if(!studentService.existsById(id)) {
			redirectAttrs.addAttribute("id", id).addFlashAttribute("errorId", "L' étudiant n'existe pas.");
			return "redirect:/allstudents";
		}
		else {
			studentService.deleteStudentById(id);
			return "redirect:/allstudents";
		}
	}
	
	@GetMapping("/choice-course")
	public String choiceCourse(Model model) {
		List<StudentDto> studentDtoList = studentService.listeStudents();
		List<CourseDto> courseDtoList = courseService.listeCourses();
		ChoiceCourseDtoFrom form = new ChoiceCourseDtoFrom();
		model.addAttribute("students", studentDtoList);
		model.addAttribute("courses", courseDtoList);
		model.addAttribute("form", form);
		return "addcourse";
	}
	
	@PostMapping("/choice/save")
	public String choiceRegistration(@ModelAttribute("form") ChoiceCourseDtoFrom choiceCourse, Model model) {
		//on prépare les données
		Student student = studentService.getStudentTrueById(choiceCourse.getStudent().getId());
		Course course = courseService.getCoursesById(choiceCourse.getCourse().getId());
		// on prépare les listes
		List<Student> studentList = course.getListStudents();		
		studentList.add(student);		
		//on ajoute dans les listes		
		course.setListStudents(studentList);
		//on enregistre		
		courseService.saveCourse(course);
		return "success-choice";
	}


}

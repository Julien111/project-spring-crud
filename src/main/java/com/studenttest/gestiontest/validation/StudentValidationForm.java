package com.studenttest.gestiontest.validation;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StudentValidationForm {
	
	@NotNull(message = "Le champ ne doit pas être null")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    @Size(min = 3, max = 60, message 
      = "Le texte doit avoir entre 2 and 60 caractères")
    private String firstName;
    
    @NotNull(message = "Le champ ne doit pas être null")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    @Size(min = 3, max = 60, message 
      = "Le texte doit avoir entre 2 and 60 caractères")
    private String lastName;
    
    @NotNull(message = "Le champ ne doit pas être null")
    @NotEmpty(message = "Le champ ne peut pas être vide")
    @Email
    private String email;
    

	public StudentValidationForm() {
		super();		
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "StudentValidationForm [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + "]";
	}    
}
